# Node-Express

## Clone this repo with HTTPS

```
https://gitlab.com/leviatan21/node-express.git
```

## Clone this repo with SSH

```
git@gitlab.com:leviatan21/node-express.git
```

## Branches

### Miguel Angel Duran

```
git checkout midudev-esmodules
git checkout midudev-commonjs
git checkout midudev-mvc
```

### Fernando Herrera Udemy

```
git checkout fherrera
```

### A Modern implementation

```
git checkout modern
git checkout modern-errorshandler
```

### A base implementation

```
git checkout base
```

### A API RESTful NodeJs - ExpressJs - MongoDB

```
git checkout APIRESTfulMongo
```
